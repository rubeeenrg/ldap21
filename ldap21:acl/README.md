# LDAP Server
## Rubén Rodríguez ASIX M06-ASO 2021-2022
### Servidor LDAP (Debian 11)

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **rubeeenrg/ldap21:acl** Servidor LDAP amb la base de dades edt.org.
     * Eliminats els schemas innecessàris.
     * Definida la base de dades 'cn=config', usuari 'Sysadmin' amb passwd syskey.
       'cn=Sysadmin,cn=config'

```
docker network create 2hisix
docker build -t rubeeenrg/ldap21:acl .
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -it rubeeenrg/ldap21:acl /bin/bash
docker ps
slapcat -n0
ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'cn=config' dn
ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'cn=config' olcDatabase={1}mdb
```

* **EXERCICI 1:**
```
1) vim acl1.ldif
2) Dins d'acl1.ldif modifiquem l'arxiu i esborrem l'antiga ACL i possem una de nova amb el següent:
   access to * by * read
3) ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f acl1.ldif
4) ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
5) ldappasswd -vD 'uid=Anna,ou=usuaris,dc=edt,dc=org' -w anna -s kaka --> NO DEIXARÀ PER ELS PERMISSOS
6) ldappasswd -x -D 'uid=Anna,ou=usuaris,dc=edt,dc=org' -w anna 'uid=pau,ou=usuaris,dc=edt,dc=org' -s kakita --> NO DEIXARÀ PER ELS PERMISOS
```

* **EXERCICI 2:**
```
1) vim acl2.ldif
2) Dins d'acl2.ldif modifiquem l'arxiu i esborrem l'antiga ACL i possem una de nova amb el següent:
   access to * by * write
3) ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f acl2.ldif
4) ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
5) ldappasswd -vD 'uid=Anna,ou=usuaris,dc=edt,dc=org' -w anna -s kaka --> PODREM CANVIAR-HO JA QUE HO HEM ESPECIFICAT EN L'ACL
6) ldappasswd -x -D 'uid=Anna,ou=usuaris,dc=edt,dc=org' -w anna 'uid=pau,ou=usuaris,dc=edt,dc=org' -s pauet --> PODREM CANVIAR-HO JA QUE HO HEM ESPECIFICAT EN L'ACL
``` 

* **EXERCICI 3:**
```
1) vim acl3.ldif
2) Dins d'acl3.ldif modifiquem l'arxiu i esborrem l'antiga ACL i possem una de nova amb el següent:
   access to by self write by * read
3) ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f acl3.ldif
4) ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
5) ldappasswd -vD 'uid=Anna,ou=usuaris,dc=edt,dc=org' -w anna -s kaka --> PODREM CANVIAR-HO JA QUE HO HEM ESPECIFICAT EN L'ACL
6) ldappasswd -x -D 'uid=Anna,ou=usuaris,dc=edt,dc=org' -w anna 'uid=pau,ou=usuaris,dc=edt,dc=org' -s pauet --> NO PODREM CANVIAR-HO JA QUE HO HEM ESPECIFICAT EN L'ACL
7) ldapsearch -x -LLL -D 'uid=Anna,ou=usuaris,dc=edt,dc=org' -w anna -b 'uid=pau,ou=usuaris,dc=edt,dc=org' --> PODEM LLEGIR JA QUE HO HEM ESPECIFICAT EN L'ACL
```   

* **EXERCICI 4:**
```
1) vim acl4.ldif
2) Dins d'acl4.ldif modifiquem l'arxiu i esborrem l'antiga ACL i possem una de nova amb el següent:
   access to attrs=userPassword
  	 by self wirte
	 by * auth
   access to * by * read
3) ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f acl4.ldif
4) ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
5) ldappasswd -vD 'uid=Anna,ou=usuaris,dc=edt,dc=org' -w anna -s kaka --> PODREM CANVIAR-HO JA QUE HO HEM ESPECIFICAT EN L'ACL
6) vim anna.ldif
7) Dins d'anna.ldif, ficarem el 'changetype: modify' i cambiarem algun atribut per probar que no ens deixa.
8) ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f anna.ldif --> DONARÀ ERROR JA QUE HEM ESPECIFICAT A L'ACL QUE NO POT
9) ldappasswd -x -D 'uid=Anna,ou=usuaris,dc=edt,dc=org' -w anna 'uid=pau,ou=usuaris,dc=edt,dc=org' -s pauet --> NO PODREM CANVIAR-HO JA QUE HO HEM ESPECIFICAT EN L'ACL
10) vim altres.ldif
11) Dins d'altres.ldif, ficarem el 'changetype: modify' i cambiarem algun atribut per probar que no ens deixa.
12) ldapmodify -xv -D 'cn=Sysadmin,cn=config' -w syskey -f altres.ldif --> DONARÀ ERROR JA QUE HEM ESPECIFICAT A L'ACL QUE NO POT 
13) ldapsearch -x -LLL -D 'uid=Anna,ou=usuaris,dc=edt,dc=org' -w anna -b 'uid=pau,ou=usuaris,dc=edt,dc=org' --> PODEM LLEGIR JA QUE HO HEM ESPECIFICAT EN L'ACL
```   
