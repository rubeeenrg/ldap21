# LDAP Server
## @edt ASIX M06-ASO 2021-2022
### Servidor LDAP (Debian 11)

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **edtasixm06/ldap21:schema** Servidor LDAP amb la base de dades edt.org
   S'ha fet el següent:

   * futbolista-A.schema --> Derviat d'inetOrgPerson, structural, injectat dades de 
     dades-futbol-A.ldif

   * futbolista-B.schema --> Derivat de TOP, structural, injectat dades de dades-futbol-B.ldif

   * futbolista-C.schema --> Derviat de TOP, auxiliary, injectat dades de dades-futbol-C.ldif

   * futbolista.schema --> Document que utilitzem original que està possat dins del fitxer 'slapd.conf'

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -it rubeeenrg/ldap21:schema /bin/bash
docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisix -p 80:80 -d edtasixm06/phpldapadmin:20
``` 
