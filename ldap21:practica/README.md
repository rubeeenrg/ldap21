  GNU nano 5.4                                                   README.md                                                            
# LDAP Server
## @edt ASIX M06-ASO 2021-2022
### Servidor LDAP (Debian 11)

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **rubeeenrg/ldap21:practica** Servidor LDAP amb la base de dades edt.org
   S'ha fet el següent:

   * Dos schemas, un structural (derivat d'inetOrgPerson) i un auxiliary.
   * Nova ou creada de nom practica.
   * 3 entitats noves dins de la ou anomenada previament.
   * 3 atributs per cada objecte (boolean, foto i pdf).

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -it rubeeenrg/ldap21:practica /bin/bash
docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisix -p 80:80 -d edtasixm06/phpldapadmin:20

DINS DEL CONTAINER:
bash startup
ldapadd -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f data-practica.ldif
```
