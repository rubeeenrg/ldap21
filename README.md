# LDAP - Rubén Rodríguez García M06-ASO
# Curs 2021-2022

* **rubeeenrg/ldap21:base** container de treball base de LDAP. Organització: edt.org
* **rubeeenrg/ldap21:editat** container de treball editat de LDAP. Organització: edt.org
* **rubeeenrg/ldap21:schema** container de treball schema de LDAP. Organització: edt.org
* **rubeeenrg/ldap21:practica** container de treball pràctic de LDAP amb schemes. Organització: edt.org
* **rubeeenrg/ldap21:groups** container de treball amb creació de grups de LDAP. Organització: edt.org
* **rubeeenrg/ldap21:acl** container de treball amb exemples ACLs de LDAP. Organització: edt.org
