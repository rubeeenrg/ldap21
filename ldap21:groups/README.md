# LDAP Server
## Rubén Rodríguez ASIX M06-ASO 2021-2022
### Servidor LDAP (Debian 11)

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **rubeeenrg/ldap21:groups** Servidor LDAP de grups amb la base de dades edt.org.
     S' ha fet el següent:
     * Modificat l'arxiu 'edt.org.ldif' amb una nova 'ou' de nom 'grups' afegida.
     * Nous grups definits: alumnes(600), professors(601), 1asix(610), 2asix(611), wheel(10), 1wiam(612), 2wiam(613), 1hiaw(614). 
     * Verificació del llistat d'usuaris i grups i la coherència de dades entre els usuaris que ja teníem creats i els nous grups creats.

```
docker network create 2hisix
docker build -t rubeeenrg/ldap21:groups .
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d rubeeenrg/ldap21:editat
docker ps
ldapsearch -x -LLL -h ldap.edt.org -b 'dc=edt,dc=org'
``` 
