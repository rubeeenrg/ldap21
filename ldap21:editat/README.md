# LDAP Server
## Rubén Rodríguez ASIX M06-ASO 2021-2022
### Servidor LDAP (Debian 11)

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **rubeeenrg/ldap21:editat** Servidor LDAP editat amb la base de dades edt.org.
     * Tots els usuaris amb identificació per uid.
     * Organització nova.
     * Dos usuaris nous amb els seus passwords encriptats.
     * Password de l'admin encriptat. 
     * Configuració client afegida.
     * Nova network creada.
     * Container propagant amb el port 389:389.

```
docker network create 2hisix
docker build -t rubeeenrg/ldap21:editat .
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisix -p 389:389 -d rubeeenrg/ldap21:editat
docker ps
ldapsearch -x -LLL -h ldap.edt.org -b 'dc=edt,dc=org'
``` 
